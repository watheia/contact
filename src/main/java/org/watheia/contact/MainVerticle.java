package org.watheia.contact;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;

public class MainVerticle extends AbstractVerticle {

  static final int PORT = 8080;

  static final String HOST = "localhost";

  @Override
  public void start(final Promise<Void> startPromise) throws Exception {
    this.vertx.createHttpServer().requestHandler(req -> {
      req.response()
          .putHeader("content-type", "text/plain")
          .end("Hello from Vert.x!");
    }).listen(PORT, HOST, http -> {
      if (http.succeeded()) {
        startPromise.complete();
        System.out.println(String.format("HTTP server started on http://%s:%d", HOST, PORT));
      } else {
        startPromise.fail(http.cause());
      }
    });
  }
}
